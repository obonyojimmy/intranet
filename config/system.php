<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Title
    |--------------------------------------------------------------------------
    |
    | 
    |
    */
    'title' => 'Intranet | Deutsch-Tansanische Partnerschaft e.V.',
    
    'email' => 'intranet@dtpev.de',
    
    'joomla_path' => env('SYSTEM_JOOMLA_PATH', '/vagrant/bridge/joomla'),
    'joomla_url' => env('SYSTEM_JOOMLA_URL', '192.168.22.10.xip.io')
    
];

