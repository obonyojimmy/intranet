FORMAT: 1A

# Example

# AppApiControllersAuthController

# AppApiControllersUploadsController

# Reports [/resources/reports]
Report resource representation

## Show all reports [GET /resources/reports]
Get a JSON representation of all reports

## Create a report [POST /resources/reports]


+ Request (application/x-www-form-urlencoded)
    + Body

            title=string&content=text&pdf=file

+ Response 201 (application/json)
    + Body

            {
                "id": "10"
            }

## Display the specified report [GET /resources/reports/10]


## Update the specified report [PUT /resources/reports/10]


## Remove the specified resource [DELETE /resources/reports/10]


+ Response 200 (application/json)
    + Body

            "1"

# AppApiControllersUsersController