module.exports = {
  configRouter: function (router) {
    router.map({
		'/projects': {
        component: require('./components/projects/index.vue'),
        subRoutes: {
		  '/': {
			
			component: require('./components/projects/posts.vue'),
		  },
		  '/post/:id': {
			name: 'singleproject',
			component: require('./components/projects/single.vue'),
		  },
		  '/edit/:id': {
			name: 'editproject',
			component: require('./components/projects/edit.vue'),
		  },
		  '/new': {
			
			component: require('./components/projects/create.vue'),
		  },
	    },
      },
		'/search': {
        component: require('./components/search/index.vue')
      },
      '/single': {
        component: require('./components/search/single.vue')
      },
       '/edit': {
        component: require('./components/search/edit.vue')
      },
      '/login': {
        component: require('./components/auth/login.vue')
      },
      'login/token/:token': {
        component: require('./components/auth/authenticate.vue')
      },
      '/logout': {
        component: require('./components/auth/logout.vue')
      },
      '/': {
        component: require('./components/home/index.vue'),
      },
      '*': {
        component: require('./components/404.vue')
      }
    })
  }
}
