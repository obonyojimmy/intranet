import {UsersResource} from './index'

export default {

  getMe: function () {
    return UsersResource.get({id: 'me'})
  },

}
