import Vue from 'vue'
import VueResource from 'vue-resource'

const API_ROOT = '/api/'

Vue.use(VueResource)

// Use CSRF Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');

Vue.http.interceptors.push({
  request (request) {
    // JWT Authorization
    request.headers = request.headers || {}
    if (localStorage.getItem('id_token')) {
		console.log(localStorage.getItem('id_token'));
      request.headers.Authorization = 'Bearer ' + localStorage.getItem('id_token')
    }
    return request
  },
  response (response) {
    // Return to login, if request is rejected
    if (response.status === 401 || response.status === 400) {
      // Return to login
      window.location.pathname = '/#!/login'
    }
    return response
  }
})

// RESTful
//var params = {} 
export const UsersResource = Vue.resource(API_ROOT + 'users{/id}')
export const AuthResource = Vue.resource(API_ROOT + 'auth{/id}')
export const ProgrammeResource = Vue.resource(API_ROOT + 'auth{/id}')
export const ProjectsResource = Vue.resource(API_ROOT + 'resources/projects{/id}')
export const SearchResource = Vue.resource(API_ROOT + 'search')
export const HomeResource = Vue.resource(API_ROOT + 'resources/projects')
