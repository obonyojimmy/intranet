import {ProjectsResource} from './index'

export default {

  getAll: function () {
    
    return ProjectsResource.get()
   
  },
  single: function (id) {
    return ProjectsResource.get({id: id})
  },
  edit: function (id,params) {
	  console.log(params)
    return ProjectsResource.update({id: id},params)
  },
  save: function (params) {
	  console.log(params)
    return ProjectsResource.save(params)
  },

}
