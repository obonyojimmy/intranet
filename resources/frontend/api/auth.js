import {AuthResource} from './index'

export default {

  login: function(email) {
    return AuthResource.get({id: null}, {'email': email})
  },

  authenticate: function(token) {
	 // console.log(token);
    return AuthResource.save({id: null}, {'token': token})
  },
  
  joomla: function() {
    return AuthResource.query({id: 'joomla'})
  }

}
