import api from '../../api/auth'
import {
  USERINFO_SUCCESS,
	USERINFO_FAILURE,
  ERROR_MESSAGE,
  SUCCESS_MESSAGE,
	AUTHENTICATE_SUCCESS,
  AUTHENTICATE_INVALID,
	LOGOUT_SUCCESS
} from '../mutation-types'

export const joomla = ({dispatch, router}) => {
  api.joomla().then(function(response) {
    // Send user to DTP Website
    var win = window.open('http://dtpev.de/index.php?option=com_jiwa&view=dash', '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        //Browser has blocked it
        alert('Please allow popups for this website.');
    }
  }, function (response) {
      if (response.data.errors) {
        // Validation errors
        dispatch(ERROR_MESSAGE, response.data.errors)
      } else {
        // Unknown error
        dispatch(ERROR_MESSAGE, ['Unknown error occured: \n' + response.data.message , ])
      }
  });
}

// Send a request to initiate email invitation
export const login = ({dispatch, router}, email, callback) => {
  api.login(email).then(function (response) {
      dispatch(SUCCESS_MESSAGE, true)
  }, function (response) {
      callback = response.data.errors
      if (response.data.errors) {
        // Validation errors
        dispatch(ERROR_MESSAGE, response.data.errors)
      } else {
        // Unknown error
        dispatch(ERROR_MESSAGE, ['Unknown error occured: \n' + response.data.message , ])
      }
  });
}

// Authenticate via email token and save the returned JWT
export const authenticate = ({dispatch, router}, token, redirect) => {
	console.log(token)
  api.authenticate(token).then(function(response) {
    localStorage.setItem('id_token', response.data.id_token)

    dispatch(AUTHENTICATE_SUCCESS, response.data.id_token)

    // Redirect to a specified route
    if(redirect) {
      router.go(redirect)
    }
  }, function (response) {
    dispatch(AUTHENTICATE_INVALID)
  });
}

// Logout
export const logout = ({dispatch, router}) => {
  // To log out, we just need to remove the token
  localStorage.removeItem('id_token')

  dispatch(LOGOUT_SUCCESS)

  // Send user to startpage
  router.go('/')
}

// Load user information
export const getUserInfo = ({ dispatch }) => {
  Users.getMe().then(response => {
    if( ! response.ok){
      return dispatch(types.USERINFO_FAILURE)
    }
    dispatch(types.USERINFO_SUCCESS, { user: response.data })
  }, response => {
    dispatch(types.USERINFO_FAILURE)
  })
}
