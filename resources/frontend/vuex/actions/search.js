
import {
  CLEAR_SEARCH_RESULTS,
  ADD_SEARCH_RESULTS
	
} from '../mutation-types'

// Load user information
export const clearsearch = ({ dispatch }) => {
  
    dispatch(CLEAR_SEARCH_RESULTS)
 
}
export const addsearch = ({ dispatch },content) => {
  
    dispatch(ADD_SEARCH_RESULTS,content)
 
}
