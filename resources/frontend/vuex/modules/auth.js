import {
	USERINFO_SUCCESS,
	USERINFO_FAILURE,
	AUTHENTICATE_SUCCESS,
	AUTHENTICATE_INVALID,
	LOGOUT_SUCCESS
} from '../mutation-types'

const state = {
	invalid: false,
  token: localStorage.getItem('id_token') || null,
  user: null
}

const mutations = {

	[AUTHENTICATE_SUCCESS](state , token){
		state.invalid = false,
    state.token = token
  },
	[AUTHENTICATE_INVALID](state) {
		state.invalid = true
	},
  [LOGOUT_SUCCESS](state){
    state.user = null
    state.token = null
  },
  [USERINFO_SUCCESS](state,action){
    state.user = action.user
  },
  [USERINFO_FAILURE](state,action){
    state.user = null
  },

}

export default {
  state,
  mutations
}
