import {
	ERROR_MESSAGE,
	SUCCESS_MESSAGE,
	CLEAR_SUCCESS_MESSAGE,
  CLEAR_ERROR_MESSAGE,
	CLEAR_MESSAGES
} from '../mutation-types'

const state = {
  success: '',
	error: '',
}

const mutations = {

	[ERROR_MESSAGE](state, content){
		state.success = ''
    state.error = content
  },
	[SUCCESS_MESSAGE](state, content){
		state.error = ''
    state.success = content
  },
	[CLEAR_ERROR_MESSAGE](state){
    state.error = ''
  },
	[CLEAR_SUCCESS_MESSAGE](state){
    state.success = ''
  },
	[CLEAR_MESSAGES](state){
		state.success = ''
    state.error = ''
  },
}

export default {
  state,
  mutations
}
