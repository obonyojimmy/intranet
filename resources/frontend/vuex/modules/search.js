import {
	CLEAR_SEARCH_RESULTS,
	ADD_SEARCH_RESULTS
} from '../mutation-types'

const state = {
	search: [],
 
}

const mutations = {

	[CLEAR_SEARCH_RESULTS](state) {
		state.search = []
	},
	[ADD_SEARCH_RESULTS](state,content) {
		state.search = content
	},
  
}

export default {
  state,
  mutations
}
