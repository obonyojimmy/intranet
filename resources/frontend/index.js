/*
| Frontend
|
| @author    Frithjof Gressmann, noc.io
└ */

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueValidator from 'vue-validator'
import { sync } from 'vuex-router-sync'
import store from './vuex/store'


import { configRouter } from './routes'
import App from './components/App.vue'

Vue.use(VueRouter)
Vue.use(VueValidator)
Vue.use(VueResource)

export var router = new VueRouter({
  saveScrollPosition: true
})

configRouter(router)

// Sync up router and store
sync(store, router)

// ... here we go!
router.start(App, '#app')
