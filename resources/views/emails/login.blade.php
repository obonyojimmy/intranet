@extends('beautymail::templates.sunny')

@section('content')

    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Nearly there!',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')

        <p>You've requested a passwordless login. Please proceed by clicking this link:</p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
            'title' => 'Login',
            'link' => $url
    ])
    
    @include('beautymail::templates.sunny.contentStart')

        <p>A few things to notice
        <ul>
            <li>This login link will be valid just once and for the next 15 minutes</li>
            <li>If you click it, you'll be logged in with the browser you are using</li>
            <li>If you haven't requested the login yourself, just ignore this mail</li>
        </ul>
</p>

    @include('beautymail::templates.sunny.contentEnd')
    
    

@stop