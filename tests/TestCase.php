<?php

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';
    

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Displays response debug information
     */
    public function debug_api_call() {
        $response = json_decode($this->response->getContent());

        if ( ! isset($response->status_code)) {
            // Seems as all is working fine ...
            dd($this->getResponseContent());
        }

        // Ok, we got an error!
        echo ('Message: ' . $response->message . "\n" .
           'Code:' . $response->status_code . "\n" .
            'debug ' . $response->debug->line . ' ' . $response->debug->file . ' ' . $response->debug->class);

        dd($response);
    }

    /**
     * Returns the json decoded response object
     *
     * @return object Json decoded response object
     */
    public function getResponseContent($as_array = false) {
        return json_decode($this->response->getContent(), $as_array);
    }
    
}
