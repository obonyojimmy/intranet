<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Index;

abstract class ResourceTest extends TestCase
{
    use DatabaseTransactions;
    use UserAuthorization;
    use FakedData;

    abstract public function getFakeData();

    abstract public function getResourceName();

    protected function getResourceEndpoint($id = false) {
        $url = 'api/resources/' . strtolower($this->getResourceName()) . 's';

        if ($id) {
            $url .= '/' . $id;
        }

        return $url;
    }

    protected function getResourceTable() {
        return strtolower($this->getResourceName()) . 's';
    }

    protected function getModelName() {
        return 'App\\' . $this->getResourceName();
    }

    protected function getModel($data = null) {
        $facade = $this->getModelName();

        return new $facade($data);
    }

    public function create_resource($data = null) {
        if ( ! $data) {
            $data = $this->getFakeData();
        }

        $this->call('POST',  $this->getResourceEndpoint(),
            isset($data['data']) ? $data['data'] : [],
            [],                                 // cookies
            isset($data['files']) ? $data['files'] : [],
            $this->authorization_header(true)   // server
        );

        return $this->getResponseModel();
    }

    protected function getResponseModel() {
        if ( ! isset($this->getResponseContent()->data)) {
            $this->debug_api_call();
        }

        $facade = $this->getModelName();
        return $facade::findOrFail($this->getResponseContent()->data->id);
    }

    protected function seeOnIndex($model = null) {
        if ( ! $model) {
            $model = $this->getResponseModel();
        }

        $this->seeInDatabase('index', [
            'indexable_id' => $model->id,
            'indexable_type' => get_class($model)
        ]);
    }

    protected function notSeeOnIndex($model) {
        $this->notSeeInDatabase('index', [
            'indexable_id' => $model->indexable_id,
            'indexable_type' => $model->indexable_type
        ]);
    }

    protected function assertFilesUploaded($files, $model) {
        foreach($files as $relation => $file) {
            // Files uploaded?
            $this->assertFileExists(uploads_path($model->$relation->getDiskPath()));
            // Relation OK?
            $this->assertEquals($model->$relation->file_name, $file->getClientOriginalName());
        }
    }

    protected function assertFilesRemoved($files) {
        foreach($files as $relation => $file) {
            // Database entries removed?
            $this->notSeeInDatabase('uploads', ['file_name' => $file->getFilename()]);
            // Files removed?
            $this->assertFileNotExists(uploads_path($file->getFilename()));
        }
    }

    /** @test */
    public function it_can_be_created() {
        $data = $this->getFakeData();

        $this->create_resource($data);

        $this->assertEquals(201, $this->response->getStatusCode());

        // Database entries created?
        $this->seeInDatabase($this->getResourceTable(), $data['data']);

        $this->seeOnIndex();

        $this->assertFilesUploaded($data['files'], $this->getResponseModel());
    }

    /** @test */
    public function it_can_be_updated() {
        $original = $this->getFakeData();
        $update = $this->getFakeData();
        $item = $this->create_resource($original);

        $this->call('PUT', $this->getResourceEndpoint($item->id),
            $update['data'],                       // data
            [],                                    // cookies
            $update['files'],                      // files
            $this->authorization_header(true)      // server
        );

        $this->assertResponseOk();

        // Database entries updated?
        $this->seeInDatabase($this->getResourceTable(), $update['data']);
        $this->seeOnIndex();

        // File uploads OK?
        $this->assertFilesRemoved($original['files']);
        $this->assertFilesUploaded($update['files'], $this->getResponseModel());
    }


    /** @test */
    public function it_can_be_deleted() {
        $data = $this->getFakeData();

        $item = $this->create_resource($data);

        $index = $item->index;

        $this->delete($this->getResourceEndpoint($item->id), [], $this->authorization_header());

        $this->assertResponseOk();

        $this->notSeeInDatabase($this->getResourceTable(), $data['data']);

        // TODO: Fix it?! Heisenbug?
        //$this->notSeeOnIndex($index);

        $this->assertFilesRemoved($data['files']);
    }

}