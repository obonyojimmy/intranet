<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthTest extends TestCase
{
    
    use DatabaseTransactions;
    use MailTracking;
    use UserAuthorization;

    /** @test */
    public function it_returns_user_information() {

        $this->get('api/auth/info', $this->authorization_header());

        $this->assertResponseOk();

        foreach (['id', 'email', 'name'] as $field) {
            $this->assertEquals($this->user->$field, json_decode($this->response->getContent())->user->$field);
        }
    }

    /** @test */
    public function it_sends_existing_user_invites() {

        $user = factory(App\User::class)->create();

        $this->get('api/auth?email=' . $user->email);

        $this->assertResponseOk();

        $this->seeEmailTo($user->email);
        $this->seeEmailSubjectContains('Your login');

        return $user;
    }
    
    /** @test */
    public function it_rejects_invalid_emails() {
        $this->get('api/auth?email=mailformed');

        $this->assertResponseStatus(422); // Unprocessable Entity

        $this->get('api/auth?email=notexisting@user.de');

        $this->assertResponseStatus(422); // Unprocessable Entity
    }

    /** @test */
    public function it_authorizes_invited_users() {
        $user = $this->it_sends_existing_user_invites();
        
        // Find the invite token (I know, DOM parsing with Regex is bad!)
        preg_match('/token\/([^"\'>]+)["\']?/', $this->getEmail()->getBody(), $match);
        $token = $match[1];

        $this->seeInDatabase('login_tokens', ['token' => $token]);

        $this->post('api/auth', ['token' => $token]);
        $this->assertResponseOk();

        $authorized_user = JWTAuth::setToken($this->getResponseContent()->id_token)->authenticate();

        $this->assertEquals($user->email, $authorized_user->email);
    }

}
