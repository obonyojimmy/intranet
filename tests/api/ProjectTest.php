<?php

class ProjectTest extends ResourceTest
{

    public function getResourceName() {
        return 'Project';
    }

    public function getFakeData() {
        return [
            'data' => [
                'title' => $this->faker->sentence,
                'description' => $this->faker->realText
            ],
            'files' => []
        ];
    }

}