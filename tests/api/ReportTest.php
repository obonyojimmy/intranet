<?php

use App\Report;

class ReportTest extends ResourceTest
{

    public function getResourceName() {
        return 'Report';
    }

    public function getFakeData() {
        return [
            'data' => [
                'title' => $this->faker->sentence,
                'content' => $this->faker->realText
            ],
            'files' => [
                'pdf' => $this->fake_upload_pdf()
            ]
        ];
    }
            
}
