<?php

use Faker\Factory as Faker;
use   \Illuminate\Http\UploadedFile;

trait FakedData {

    /**
     * @var Faker\Generator
     */
    protected $faker;

    /** @before */
    public function setUpFaker()
    {
        $this->faker = Faker::create();
    }

    /**
     * Returns a mocked up file for fileupload testing
     *
     * @return UploadedFile
     */
    public function fake_upload($file, $name = null) {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $size = filesize(storage_path('mockup/' . $file));

        if ( ! $name) {
            $name = $this->faker->name . '.' . $extension;
        }

        // Copy the PDF to tmp folder
        $tmp_name = str_random() . '.pdf';
        Storage::disk('storage')->copy('mockup/' . $file, 'mockup/tmp/' . $tmp_name);

        return new UploadedFile(
            storage_path('mockup/tmp/' . $tmp_name),
            $name,
            'application/pdf',
            $size,
            null,
            true
        );
    }

    /**
     * Returns a mocked up PDF
     *
     * @param null $name
     * @return UploadedFile
     */
    public function fake_upload_pdf($name = null) {
        return $this->fake_upload('Donet_School_project_small_report.pdf', $name);
    }

    /**
     * Returns a mocked up DOCX
     *
     * @param null $name
     * @return UploadedFile
     */
    public function fake_upload_docx($name = null) {
        return $this->fake_upload('Januar_2014_Mwanza_Englisch_Bukoba_Trip.docx', $name);
    }

    /** @after */
    public function cleanup() {
        // @TODO fix for multiple test working
        return;
        if (Storage::disk('storage')->exists('mockup/tmp')) {
            Storage::disk('storage')->deleteDirectory('mockup/tmp');
        }
        if (Storage::disk('storage')->exists('mockup/uploads')) {
            Storage::disk('storage')->deleteDirectory('mockup/uploads');
        }
    }

}