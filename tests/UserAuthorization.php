<?php

use Tymon\JWTAuth\Facades\JWTAuth;

trait UserAuthorization
{

    /**
     * Stores an authorization token
     *
     * @var mixed
     */
    protected $token = false;

    /**
     * Stores the authorized user if existing
     *
     * @var mixed
     */
    protected $user = false;

    /**
     * Sign the user in
     *
     * @return
     */
    public function signin() {

        if ( ! $this->user) {
            $this->user = factory(App\User::class)->create();
        }

        return $this->actingAs($this->user);
    }

    /**
     * Requests a token
     * @return string JWT token
     */
    public function authorize() {
        if ( ! $this->user) {
            $this->user = factory(App\User::class)->create();
        }

        $this->token = JWTAuth::fromUser($this->user);

        return $this->token;
    }

    /**
     * Returns an authorization header
     *
     * @param boolean $server If true $_SERVER format will be returned
     * @return array Authorization header
     */
    public function authorization_header($server = false) {
        if ( ! $this->token) {
            $this->authorize();
        }

        if ($server) {
            return [
                'HTTP_AUTHORIZATION' => 'Bearer ' . $this->token
            ];
        }

        return [
            'Authorization' => 'Bearer ' . $this->token
        ];
    }

}