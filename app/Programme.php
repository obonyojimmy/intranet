<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programme extends Model
{
    use ManagesIndex;
    use ManagesActivity;
    
    public function getIndexTitle() {
        return $this->name;
    }

    public function getIndexContent() {
        return $this->description;
    }

    public function volunteers() {
        return $this->hasManyThrough('App\Volunteer', 'App\Intake');
    }
}
