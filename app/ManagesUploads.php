<?php

namespace App;


trait ManagesUploads
{

    /**
     * Processes upload files
     *
     * @param $model
     * @param $request
     */
    public function saveUploads() {

        foreach ($this->getUploadFields() as $field) {

            $upload = ($this->$field) ? $this->$field : new Upload();

            if ( ! $upload->fromRequest($field)) {
                continue;
            }

            $this->$field()->save($upload);
        }

    }

    /**
     * Deletes upload relations
     */
    public function deleteUploads() {
        foreach ($this->getUploadFields() as $field) {
            $this->$field()->delete();
        }
    }

    /**
     * Get the upload fields
     *
     * @return array
     */
    protected function getUploadFields()
    {
        if ( ! isset($this->uploadFields)) {
            return [];
        }

        return $this->uploadFields;
    }


    /**
     * Register the necessary event listeners.
     *
     * @return void
     */
    protected static function bootManagesUploads()
    {
        // Process file uploads with high priority
        static::saved(function ($model) {
            $model->saveUploads();
        }, 100);

        // Delete upload relations
        static::deleting(function($model) {
            $model->deleteUploads();
        });
    }

}
