<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{

    use ManagesIndex;
    use ManagesActivity;

    protected $fillable = ['title', 'description'];

    public function hosting_places() {
        return $this->belongsToMany('App\HostingPlace');
    }

    public function reports() {
        return $this->morphMany('App\Report');
    }

    public function getIndexTitle() {
        return $this->title;
    }

    public function getIndexContent() {
        $content = $this->description;

        return $content;
    }

}
