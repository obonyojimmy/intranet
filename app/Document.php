<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{

    use ManagesActivity;
    use ManagesIndex;

    public function getIndexTitle() {
        return $this->title;
    }

    public function getIndexContent() {
        return $this->content;
    }

}
