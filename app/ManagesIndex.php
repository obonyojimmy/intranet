<?php

namespace App;

use ReflectionClass;

trait ManagesIndex
{

    /**
     * Index relation
     *
     * @return \App\Index
     */
    public function index() {
        return $this->morphOne('App\Index', 'indexable');
    }

    /**
     * Register the necessary event listeners.
     *
     * @return void
     */
    protected static function bootManagesIndex()
    {

        // Update the index on save
        static::saved(function ($model) {
            $model->index()->save(new Index([
                'title' => $model->getIndexTitle(),
                'content' => $model->getIndexContent()
            ]));
        });

        // Removing from index on delete
        static::deleting(function($model) {
            $model->index()->delete();
        });
        
    }

    /**
     * @return string title
     */
    abstract function getIndexTitle();

    /**
     * @return string content
     */
    abstract function getIndexContent();
}
