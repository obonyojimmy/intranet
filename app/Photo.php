<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use ManagesActivity;
    use ManagesIndex;
    use ManagesUploads;

    protected $uploadFields = ['image'];

    public function image() {
        return $this->morphOne('App\Upload', 'uploadable');
    }

    public function getIndexTitle() {
        return $this->title;
    }

    public function getIndexContent() {
        $content = $this->description . "\n";
        $content .= $this->photographer;

        return $content;
    }
}
