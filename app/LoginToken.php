<?php

namespace App;

use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LoginToken extends Model
{
    /**
     * Fillable fields for the model.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'token'];

    /**
     * Generate a new token for the given user.
     *
     * @param  User $user
     * @return $this
     */
    public static function generateFor(User $user)
    {
        return static::create([
            'user_id' => $user->id,
            'token'   => str_random(50)
        ]);
    }

    /**
     * Get the route key for implicit model binding.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'token';
    }
    
    /**
     * Check if unexpired token exists
     * @param string $token
     * @return type
     */
    public static function validate($token) {
        return self::where('token', $token)
            ->where('created_at', '>', Carbon::parse('-15 minutes'))
            ->firstOrFail();
            //dd($token);
           // $token = '$2y$10$zBlHfqS6igVQq8x4cKDn.OQKpgZezt91aoWgq058FMymKs3DR/PVW';
        //return self::where('token', $token)->first();
            
            
    }
    
    /**
     * Clears all expired unused tokens
     * @return int Number of deleted expired tokens
     */
    public static function clearExpired() {
        return self::where('created_at', '<', Carbon::parse('-16 minutes'))
            ->delete();
    }

    /**
     * Send the token to the user.
     */
    public function send()
    {
        $url = \Config::get('app.url') . '/#!/login/token/' . $this->token;

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $beautymail->send('emails/login', ['url' => $url], function($message)
        {
            $message
                ->from(config('system.email'))
                ->to($this->user->email)
                ->subject('Your login | ' . \Config::get('system.title'));
        });
    }

    /**
     * A token belongs to a registered user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
