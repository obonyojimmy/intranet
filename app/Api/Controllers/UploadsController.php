<?php

namespace App\Api\Controllers;

use \App\Upload;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;


class UploadsController extends BaseController
{

    public function download($id) {
        
        $upload = Upload::findOrFail($id);
        
        if ( ! upload_exists($upload->filename) ) {
            return $this->response->errorNotFound();
        }

        $file = Storage::disk('uploads')->get($upload->filename);

        return (new Response($file, 200))
            ->header('Content-Disposition', 'attachment; filename="' . $upload->filename . '"')
            ->header('Content-Type', $upload->mime);
    }
    
}