<?php namespace App\Api\Controllers;

use App\Project;
use App\Http\Requests;
use App\Api\Requests\ProjectRequest;
use App\Api\Transformers\ProjectTransformer;

/**
 * Project resource representation
 *
 * @Resource("Projects", uri="/resources/projects")
 */
class ProjectsController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Show all projects, paginated
     *
     * Get a JSON representation of all projects
     *
     * @Get("/")
     */
    public function index()
    {
		//dd('index');
       $projects = Project::paginate(10);
       return $this->response->paginator($projects, new ProjectTransformer);
       //return 'jimmy';
    }

    /**
     * Create a project
     *
     * @Post("/")
     * @Request("title=string&content=text&pdf=file", contentType="application/x-www-form-urlencoded")
     * @Response(201, body={"id": "10"})
     */
    public function store(ProjectRequest $request)
    {
        $project = Project::create($request->only(['title', 'description']));

        return $this->item($project, new ProjectTransformer)->statusCode(201);
    }

    /**
     * Display the specified project
     *
     * @Get("/10")
     */
    public function show($id)
    {
        return $this->item(Project::findOrFail($id), new ProjectTransformer);
    }

    /**
     * Update the specified project
     *
     * @Put("/10")
     */
    public function update(ProjectRequest $request, $id)
    {
        $project = Project::findOrFail($id);

        $project->update($request->only(['title', 'description']));

        return $this->item($project, new ProjectTransformer);
    }

    /**
     * Remove the specified resource
     *
     * @Delete("/10")
     * @Response(200, body="1")
     */
    public function destroy($id)
    {
        return Project::destroy($id);
    }

}
