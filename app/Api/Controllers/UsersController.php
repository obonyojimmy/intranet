<?php

namespace App\Api\Controllers;

use Auth;
use \App\User;
use Illuminate\Http\Request;

class UsersController extends BaseController
{

    public function __construct() {
        
        // For the time being, we only allow Joomla with an user_id of 1 to manage users
        //~ if (Auth::guard('api')->user()->id != 1) {
            //~ abort(403, 'Unauthorized action.');
        //~ }
        
    }
    
    public function index() {
        // Not implemented yet
    }
    
    public function show(User $user) {
        // Not implemented yet
    }
    
    public function update(User $user, Request $request) {

        $user->email = $request->input('email');
        $user->name = $request->input('name');
        
        $user->save();
        
        return response()->json(['message' => 'User successfully updated.']);
        
    }
    
    public function store(Request $request) {
        
        $user = new User($request->only('email', 'name'));
        
        // Generate an API token
        $user->api_token = str_random(60);
        
        $user->save();
        
        return response()->json(['message' => 'User successfully created.']);
    }
    
    /**
     * Removes a user from the database
     * @param User $user
     * @return type
     */
    public function destroy(User $user) {
        
        $user->delete();
        
        return response()->json(['message' => 'User successfully removed.']);
    }
    
}
