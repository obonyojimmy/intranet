<?php

namespace App\Api\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\Upload;

class BaseController extends Controller
{
    use Helpers;

}