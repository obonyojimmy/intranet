<?php

namespace App\Api\Controllers;

use App\User;
use App\AuthenticatesUser;
use Dingo\Api\Facade\API;
use Illuminate\Http\Request;
use App\Api\Requests\UserRequest;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends BaseController
{
    
    /**
     * @var AuthenticatesUser
     */
    protected $auth;
    
    /**
     * Create a new controller instance.
     *
     * @param AuthenticatesUser $auth
     */
    public function __construct(AuthenticatesUser $auth)
    {
        $this->auth = $auth;
    }
    
    /**
     * Returns the current logged in user
     * @param Request $request
     * @return type
     */
    public function info(Request $request)
    {
        return JWTAuth::parseToken()->authenticate();
    }
    
    public function login() {
        $this->auth->invite();
        
        return response()->json(['message' => 'Go check your mail!']);
    }

    public function authenticate(Request $request)
    {
	    
        $token = \App\LoginToken::validate($request->input('token'));
        //dd($token);
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $user = $this->auth->byToken($token)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
            // Generate JWT
            $jwt = JWTAuth::fromUser($user);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(['id_token' => $jwt]);
    }

    public function validateToken() 
    {
        // Our routes file should have already authenticated this token, so we just return success here
        return API::response()->array(['status' => 'success'])->statusCode(200);
    }

    public function register(UserRequest $request)
    {
        $newUser = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
        ];
        $user = User::create($newUser);
        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }
    
    /**
     * Signs user into Joomla Application
     * @return type
     */
    public function joomla() {
        // 
        $joomlaBridge = new \App\JoomlaBridge();
        
        $joomlaBridge->authenticate();
        
        return response()->json(['message' => 'Logged in!']);
    }
    
}
