<?php namespace App\Api\Controllers;

use App\Http\Requests;
use App\Api\Transformers\ReportTransformer;
use \TomLingham\Searchy\Facades\Searchy as SearchEngine;
use Request;


/**
 * Search representation
 *
 * @Resource("Search", uri="/search")
 */
class SearchController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function getQueryTerm() {
        $query = Request::input('q', '');
        // @todo: SANATIZE!?

        return $query;
    }

    /**
     * Search on index
     * @Get("/")
     */
    public function index() {
        //~ $results = SearchEngine::search('index')
            //~ ->fields('title','body')
            //~ ->query($this->getQueryTerm())
            //~ ->getQuery()->paginate(5)
            //~ ->get();

        //~ // Transform to index collection @todo: Is that really necessary?
        //~ $collection = collect(array_map(function($result) {
            //~ return (new \App\Index())->forceFill(get_object_vars($result));
        //~ }, $results));

        //~ return $this->response->paginator($collection, new IndexTransformer);
         return [[1, 'Title', 'Content'],[1, 'Title', 'Content']];
         //return response()->json(['data' => {[1, 'Title', 'Content'],[1, 'Title', 'Content']] ]);
    }

    /**
     * Fast search for quick results
     */
    public function preview() {
        $results = SearchEngine::search('index')
            ->fields('title','body')
            ->select('title,indexable_type,indexable_id,views')
            ->query($this->getQueryTerm())
            ->getQuery()->limit(5)
            ->get();

        return $this->response->array($results);
    }

}
