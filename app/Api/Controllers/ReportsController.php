<?php namespace App\Api\Controllers;

use App\Report;
use App\Http\Requests;
use App\Api\Requests\ReportRequest;
use App\Api\Transformers\ReportTransformer;

/**
 * Report resource representation
 *
 * @Resource("Reports", uri="/resources/reports")
 */
class ReportsController extends BaseController
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Show all reports, paginated
     *
     * Get a JSON representation of all reports
     *
     * @Get("/")
     */
    public function index()
    {
        $reports = Report::paginate(10);
        return $this->response->paginator($reports, new ReportTransformer);
    }

    /**
     * Create a report
     *
     * @Post("/")
     * @Request("title=string&content=text&pdf=file", contentType="application/x-www-form-urlencoded")
     * @Response(201, body={"id": "10"})
     */
    public function store(ReportRequest $request)
    {
        $report = Report::create($request->only(['title', 'content']));

        return $this->item($report, new ReportTransformer)->statusCode(201);
    }

    /**
     * Display the specified report
     *
     * @Get("/10")
     */
    public function show($id)
    {
        return $this->item(Report::findOrFail($id), new ReportTransformer);
    }

    /**
     * Update the specified report
     *
     * @Put("/10")
     */
    public function update(ReportRequest $request, $id)
    {
        $report = Report::findOrFail($id);

        $report->update($request->only(['title', 'content']));

        return $this->item($report, new ReportTransformer);
    }

    /**
     * Remove the specified resource
     *
     * @Delete("/10")
     * @Response(200, body="1")
     */
    public function destroy($id)
    {
        return Report::destroy($id);
    }


    public function activity() {
        
    }
}
