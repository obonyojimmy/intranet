<?php

namespace App\Api\Transformers;

use App\Activity;
use League\Fractal\TransformerAbstract;

class ActivityTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'subject',
        'user'
    ];

	public function transform(Activity $activity)
	{
		return [
			'id' 	=> (int) $activity->id,
			'name' => $activity->name,
			'time' => $activity->created_at->diffForHumans()
		];
	}

    public function includeSubject(Activity $activity) {
        $subject = $activity->subject;
        $transformer = last(explode('\\', $activity->subject_type)) . 'Transformer';

        return $this->item($subject, new $transformer);
    }

    public function includeUser(Activity $activity) {
        $user = $activity->user;

        return $this->item($user, new UserTransformer);
    }
}