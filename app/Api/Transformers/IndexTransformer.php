<?php

namespace App\Api\Transformers;

use App\Index;
use League\Fractal\TransformerAbstract;

class IndexTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'indexable'
    ];

	public function transform(Index $index)
	{
		return [
			'id' 	=> (int) $index->id,
			'views' => (int) $index->views,
            'deleted' => (bool) $index->deleted_at->isPast(),
            'deleted_at' => $index->deleted_at->diffForHumans()
		];
	}

    public function includeIndexable(Index $index) {
        $indexable = $index->indexable();
        $transformer = last(explode('\\', $index->indexable_type)) . 'Transformer';

        return $this->item($indexable, new $transformer);
    }
}