<?php

namespace App\Api\Transformers;

use App\Report;
use App\Upload;
use League\Fractal\TransformerAbstract;

class ReportTransformer extends TransformerAbstract
{
	/**
	 * List of resources possible to include
	 *
	 * @var array
	 */
	protected $defaultIncludes = [
		'pdf'
	];

	public function transform(Report $report)
	{
		return [
			'id' 	=> (int) $report->id,
			'title'  => $report->title,
			'content' => $report->content
		];
	}

	/**
	 * Include PDF
	 *
	 * @return League\Fractal\ItemResource
	 */
	public function includePdf(Report $report)
	{
		$pdf = $report->pdf()->first();

		return $this->item($pdf, new UploadTransformer);
	}
}