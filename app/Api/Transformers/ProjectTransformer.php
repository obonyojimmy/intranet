<?php

namespace App\Api\Transformers;

use App\Project;
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract
{

	protected $availableIncludes = [
		'reports'
	];

	public function transform(Project $project)
	{
		return [
			'id' 	=> (int) $project->id,
			'title' => $project->title,
			'description' => $project->description
		];
	}

	public function includeReports(Project $project) {
		$reports = $project->reports()->paginate(5)->get();

		return $this->response->paginator($reports, new ReportTransformer);
	}
}