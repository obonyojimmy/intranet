<?php

namespace App\Api\Transformers;

use App\Upload;
use League\Fractal\TransformerAbstract;

class UploadTransformer extends TransformerAbstract
{

	public function transform(Upload $upload)
	{
		return [
			'id' 	=> (int) $upload->id,
			'file_name'  => $upload->file_name,
			'file_size' => (int) $upload->file_size,
			'content_type' => $upload->content_type,
			'download_url' => route('download', ['id' => (int) $upload->id])
		];
	}
}