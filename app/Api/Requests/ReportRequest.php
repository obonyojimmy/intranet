<?php

namespace App\Api\Requests;

use Dingo\Api\Http\FormRequest;

class ReportRequest extends FormRequest
{
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
            return [
	    		'title' => 'required|max:255',
	    		'content' => 'required|min:10',
				'pdf' => 'mimes:pdf'
            ];
	}
}