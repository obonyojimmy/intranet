<?php

namespace App\Api\Requests;

use Dingo\Api\Http\FormRequest;

class ProjectRequest extends FormRequest
{
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
            return [
	    		'title' => 'required|max:255',
            ];
	}
}
