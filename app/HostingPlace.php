<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostingPlace extends Model
{
    use ManagesIndex;
    use ManagesActivity;

    public function projects() {
        return $this->belongsToMany('App\Project');
    }

    public function volunteers() {
        return $this->hasMany('App\Volunteer');
    }

    public function getIndexTitle() {
        return $this->name;
    }

    public function getIndexContent() {
        $content = $this->description . "\n";
        $content .= $this->address;

        return $content;
    }
}
