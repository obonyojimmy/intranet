<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use ManagesIndex;
    use ManagesActivity;
    use ManagesUploads;

    /**
     * File upload fields
     *
     * @var array
     */
    protected $uploadFields = ['pdf'];

    /**
     * Mass assignable fields
     *
     * @var array
     */
    protected $fillable = ['title', 'content'];

    /**
     * Deactivate timestamp as they will be recorded with the index
     * @var bool
     */
    public $timestamps = false;

    /**
     * PDF Upload
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function pdf() {
        return $this->morphOne('App\Upload', 'uploadable');
    }
    
    public function subject() {
        return $this->morphTo();
    }
    
    function getIndexTitle() {

        return $this->title;
    }

    function getIndexContent() {
        $content = $this->content;
        
        // We extract the PDF text to make the upload searchable as well
        $pdf = $this->pdf()->first();
        $content .= ($pdf) ? $pdf->toText() : '';

        return $content;
    }
    
}
