<?php

if (! function_exists('uploads_path')) {

    /**
     * Get the path to the uploads folder.
     *
     * @param  string  $path
     * @return string
     */
    function uploads_path($path = '')
    {
        return config('filesystems.disks.uploads.root').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (! function_exists('upload_exists')) {

    /**
     * Checks whether an upload exists
     *
     * @param  string  $path
     * @return string
     */
    function upload_exists($path = '')
    {
        return Storage::disk('uploads')->exists($path);
    }
}






