<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intake extends Model
{
    use ManagesActivity;
    use ManagesIndex;

    public function getIndexTitle() {
        return $this->title;
    }

    public function programme() {
        return $this->belongsTo('App\Programme');
    }

    public function volunteers() {
        return $this->hasMany('App\Volunteer');
    }

    public function getIndexContent() {
        return $this->description();
    }
}
