<?php

// API

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    
	$api->group(['namespace' => 'App\Api\Controllers', 'middleware' => '\Barryvdh\Cors\HandleCors::class'], function ($api) {
	//$api->group(['namespace' => 'App\Api\Controllers', function ($api) {

		// Authentication endpoints
		
		//test api route
		$api->get('test', function () {
			return 'Hello World';
		});
		
		$api->get('auth', 'AuthController@login');
		$api->post('auth', 'AuthController@authenticate');
        
		// JWT protected endpoints
		$api->group( [ 'middleware' => 'jwt.auth' ], function ($api) {

			// Login bridge to Joomla
			$api->get('auth/joomla', 'AuthController@joomla');

			// System & Settings
			$api->get('auth/info', 'AuthController@info');

			// Uploads
			$api->get('uploads/{id}', ['as' => 'download', 'uses' => 'UploadsController@download']);
			// todo: thumbnail generation

			// Resources
			$api->resource('resources/reports', 'ReportsController');
			$api->resource('resources/projects', 'ProjectsController');
			//$api->resource('resources/programmes', 'ProjectsController');

			// Search
			$api->get('search', 'SearchController@index');
			$api->get('search/preview', 'SearchController@preview');

		});
                
		// Token-secured endpoints
		$api->group( [ 'middleware' => 'auth:api' ], function ($api) {

			$api->resource('users', 'UsersController');

		});
	});

});

// APP

Route::get('/', function () {
    return view('frontend');
});
    
