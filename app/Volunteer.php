<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    use ManagesIndex;
    use ManagesActivity;
    use ManagesUploads;

    protected $uploadFields = ['photo'];

    public function photo() {
        return $this->morphOne('App\Upload', 'uploadable');
    }

    public function projects() {
        return $this->belongsToMany('App\Project');
    }

    public function hosting_place() {
        return $this->belongsTo('App\HostingPlace');
    }

    public function intake() {
        return $this->belongsTo('App\Intake');
    }

    public function reports() {
        return $this->morphMany('App\Report');
    }

    public function getIndexTitle() {
        return $this->name;
    }

    public function getIndexContent() {
        $content = $this->description . "\n";
        $content .= $this->address;

        return $content;
    }

}
