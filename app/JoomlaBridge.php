<?php

namespace App;

use Config;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

class BridgesJoomla extends Model
{
    
    protected $app = false;
    protected $db = false;
    protected $session = false;

    /**
     * Load the Joomla Platform
     */
    public function loadFramework() {
        
        define('_JEXEC', 1);

        if (file_exists(Config::get('system.joomla_path') . '/defines.php'))
        {
            include_once Config::get('system.joomla_path') . '/defines.php';
        }

        if (!defined('_JDEFINES'))
        {
            define('JPATH_BASE', Config::get('system.joomla_path'));
            require_once JPATH_BASE . '/includes/defines.php';
        }

        require_once JPATH_BASE . '/includes/framework.php';
        
        require_once (JPATH_BASE .'/libraries/joomla/factory.php');
        
        $this->db = \JFactory::getDbo();
        $this->app = \JFactory::getApplication('site');
        
        return $this;
    }
    
    public function startSession() {
        $options = [
          'name' =>  md5(\JFactory::getConfig()->get('secret') . 'site'),
          'expire' => 900
        ];
        
	$this->session = \JFactory::getSession($options);
        $this->session->initialise($this->app->input);
        $this->session->start();
        
        return $this;
    }
    
    /**
     * Returns the User database entry
     * @param string $email Defaults to JWT Identity mail
     * @return type
     */
    public function getUser($email = false) {
        
        if ( ! $email) {
            // Use the JWT Identity
            $email = JWTAuth::parseToken()->authenticate()->email;
        }
        
        $query = $this->db->getQuery(true)
            ->select('id')
            ->from('#__users')
            ->where('email=' . $this->db->quote($email));

        $this->db->setQuery($query);
        
        $user = $this->db->loadObject();
        
        if ( ! $user) {
            abort('User is not registered in the Joomla System', 404);
        }
        
        return \JUser::getInstance($user->id);
    }
    
    /**
     * Logs the user into Joomla system
     */
    public function loginUser() {
        $user = $this->getUser();
        
        // Mark the user as logged in
        $user->set('guest', 0);
        
        // Save to session
        $this->session->set('user', $user);
        
        // Update the user related fields for the Joomla sessions table.
        $query = $this->db->getQuery(true)
            ->update($this->db->quoteName('#__session'))
                
            ->set($this->db->quoteName('guest') . ' = ' . $this->db->quote($user->guest))
            ->set($this->db->quoteName('username') . ' = ' . $this->db->quote($user->username))
            ->set($this->db->quoteName('userid') . ' = ' . (int) $user->id)
                
            ->where($this->db->quoteName('session_id') . ' = ' . $this->db->quote($this->session->getId()));

        $this->db->setQuery($query)->execute();
		
        // Hit the user last visit field
        $user->setLastVisit();
        
        return $this;
    }
    
    public function sendAuthCookie() {
        
        $this->app->input->cookie->set(
                $this->session->getName(), 
                $this->session->getId(), 
                0, 
                '/', 
                '.' . config('system.joomla_url'), 
                0
        );
        
        return true;
    }
    
    public function authenticate() {
        
        return $this->loadFramework()
                ->startSession()
                ->loginUser()
                ->sendAuthCookie();
        
    }
}
