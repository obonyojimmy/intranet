<?php

namespace App;

//use Log;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Dingo\Api\Exception\ValidationHttpException;

class AuthenticatesUser
{
    use ValidatesRequests;

    /**
     * @var Request
     */
    protected $request;

    /**
     * Create a new AuthenticatesUser instance.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Send a sign in invitation to the user.
     */
    public function invite()
    {
        $this->validateRequest()
             ->createToken()
             ->send();
    }

    /**
     * Get the user associated with a token.
     *
     * @param  LoginToken $token
     * @return void
     */
    public function byToken(LoginToken $token)
    {
		//dd($token);
		//Log::info('Showing user token: '.$token);
        $user = $token->user;

        // Token is only valid once, so remove it
        $token->delete();
        
        return $user;
    }
    
    /**
     * Ensure correct API response, see https://github.com/dingo/api/issues/584
     */
    protected function throwValidationException(\Illuminate\Http\Request $request, $validator) {
        throw new ValidationHttpException($validator->errors());
    }

    /**
     * Validate the request data.
     *
     * @return $this
     */
    protected function validateRequest()
    {
        
        $this->validate($this->request, [
            'email' => 'required|email|exists:users',
        ]);

        return $this;
    }

    /**
     * Prepare a log in token for the user.
     *
     * @return LoginToken
     */
    protected function createToken()
    {
        $user = User::byEmail($this->request->email);

        return LoginToken::generateFor($user);
    }
}
