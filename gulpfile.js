process.env.DISABLE_NOTIFIER = true

var elixir = require('laravel-elixir')
             require('laravel-elixir-vueify');

    elixir.config.sourcemaps = true;

elixir(function (mix) {
  mix.scripts([
    "../../../node_modules/jquery/dist/jquery.js",
    "../../../node_modules/semantic-ui/dist/semantic.js",
    "../../../node_modules/pace-progress/pace.js",
  ])
  .styles([
    "../../../node_modules/semantic-ui/dist/semantic.css",
    "../../../node_modules/pace-progress/themes/dtp/pace-theme-flash.css",
   ])
  .copy('node_modules/semantic-ui/dist/themes', 'public/css/themes')
  .browserify('../../frontend/index.js', 'public/js/app.js')
  .copy('resources/assets/images', 'public/images');
  
  //~ mix.browserSync({ proxy: 'http://intranet.192.168.22.10.xip.io/' });
});
