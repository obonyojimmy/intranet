<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('hosting_place_id')->unsigned();
            $table->integer('intake_id')->unsigned();
            $table->string('sex', 1);
            $table->string('name');
            $table->text('mobiles')->nullable();
            $table->text('description')->nullable();
            $table->date('birthday');
            $table->string('website')->nullable();
            $table->string('blog')->nullable();
            $table->string('skype')->nullable();
            $table->string('facebook')->nullable();
            $table->text('address')->nullable();
            $table->string('geoLat')->nullable();
            $table->string('geoLong')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('volunteers');
    }
}
