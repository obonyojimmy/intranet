<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('index', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('indexable_id');
            $table->string('indexable_type');
            $table->string('title');
            $table->text('content');
            $table->integer('views')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        // $table->engine = 'MyISAM';
        //DB::statement('ALTER TABLE index ADD FULLTEXT search(title, content)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::table('index', function($table) {
        //    $table->dropIndex('search');
        //});
        Schema::drop('index');
    }
}
